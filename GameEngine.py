import os
import random

from Buildings import Building, MissileBase
from Missiles import Missile, State


SCREEN_WIDTH, SCREEN_HEIGHT = 1200, 800
BASE_X, BASE_Y, ENEMY_LINE = 0, -300, 400
TRACER = 7
SHOTS_SCATTER = 50
BUILDINGS_INFO = {'house': [BASE_X - 200, BASE_Y],
                  'kremlin': [BASE_X - 400, BASE_Y],
                  'nuclear': [BASE_X + 200, BASE_Y],
                  'skyscraper': [BASE_X + 400, BASE_Y]}


class Engine:

    def __init__(self, window, max_enemy_missiles, game_over_pen):
        self.window = window
        self.set_window(self.window)

        self.our_missiles = []
        self.enemy_missiles = []
        self.buildings = self.append_buildings(base_x=BASE_X, base_y=BASE_Y)

        self.play(max_enemy_missiles, game_over_pen)

    def play(self, max_enemy_missiles, game_over_pen):
        self.window.onclick(self.fire_friendly_missile)

        while not self.game_over():
            self.window.update()
            self.redraw_buildings()
            self.enemy_fire_if_needed(max_enemy_missiles)

            self.check_impact()
            self.check_interception()

            self.move_missiles(self.our_missiles)
            self.clear_dead(self.our_missiles)

            self.move_missiles(self.enemy_missiles)
            self.clear_dead(self.enemy_missiles)

        game_over_pen.write('game over', align="center", font=["Arial", 80, "bold"])

    @staticmethod
    def set_window(window):
        window.clear()
        window.tracer(n=TRACER)
        window.setup(SCREEN_WIDTH + 3, SCREEN_HEIGHT + 3)
        window.screensize(SCREEN_WIDTH, SCREEN_HEIGHT)

        background_picture_name = os.path.join(os.getcwd(), 'images', 'background.png')
        window.bgpic(background_picture_name)

        return window

    def append_buildings(self, base_x, base_y):
        base = MissileBase(window=self.window, pos_x=base_x, pos_y=base_y, name='base')
        buildings = [base]

        for name, position in BUILDINGS_INFO.items():
            building = Building(window=self.window, pos_x=position[0], pos_y=position[1], name=name)
            buildings.append(building)

        return buildings

    def fire_friendly_missile(self, target_x, target_y):
        missile = Missile(pos_x=BASE_X,
                          pos_y=BASE_Y,
                          color='white',
                          target_x=target_x,
                          target_y=target_y)

        self.our_missiles.append(missile)

    def fire_enemy_missile(self):
        missile_pos = random.randint(-SCREEN_WIDTH / 2, SCREEN_WIDTH / 2)
        shot_accuracy = random.randint(-SHOTS_SCATTER, SHOTS_SCATTER)
        alive_buildings = self.find_alive_buildings()
        hit_x, hit_y = self.pick_random_target(alive_buildings, shot_accuracy)

        new_missile = Missile(pos_x=missile_pos,
                              pos_y=ENEMY_LINE,
                              color='red',
                              target_x=hit_x,
                              target_y=hit_y)
        self.enemy_missiles.append(new_missile)

    def find_alive_buildings(self):
        alive_buildings = []
        for building in self.buildings:
            if building.is_alive:
                alive_buildings.append(building)
        return alive_buildings

    @staticmethod
    def pick_random_target(alive_buildings, shot_precision):
        if alive_buildings:
            target = random.choice(alive_buildings)
            hit_x = target.pos_x + shot_precision
            hit_y = target.pos_y + shot_precision
            return hit_x, hit_y
        return 0, 0

    @staticmethod
    def move_missiles(missiles):
        for missile in missiles:
            missile.step()

    @staticmethod
    def clear_dead(missiles):
        for missile in missiles:
            if missile.state == State.dead:
                missiles.remove(missile)

    def check_impact(self):
        for missile in self.enemy_missiles:
            if missile.state == State.exploding:
                for building in self.buildings:
                    distance_to_target = missile.get_distance(building.pos_x,
                                                              building.pos_y)
                    if distance_to_target < missile.radius * 10:
                        if building.health > 0:
                            building.health -= 1

    def redraw_buildings(self):
        for building in self.buildings:
            building.set_shapes()

    def check_interception(self):
        for our_missile in self.our_missiles:
            if our_missile.state == State.exploding:
                for enemy_missile in self.enemy_missiles:
                    distance_to_target = enemy_missile.get_distance(our_missile.pos_x,
                                                                    our_missile.pos_y)
                    if distance_to_target < our_missile.radius * 10:
                        enemy_missile.evaporate()

    def enemy_fire_if_needed(self, max_enemy_missiles):
        if len(self.enemy_missiles) < max_enemy_missiles:
            self.fire_enemy_missile()

    def game_over(self):
        return self.buildings[0].health == 0
