import turtle

import GameEngine
from TurtleHelper import Helper

window = turtle.Screen()
game_over_label = Helper.create_turtle('red')

while True:
    engine = GameEngine.Engine(window=window,
                               max_enemy_missiles=5,
                               game_over_pen=game_over_label)
    answer = window.textinput(title='Missile Command', prompt='Хотите сыграть еще? д/н')
    if answer.lower() not in ('д', 'да', 'y', 'yes'):
        break
