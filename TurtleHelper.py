import turtle


class Helper:

    @staticmethod
    def create_turtle(color):
        pen = turtle.Turtle(visible=False)
        pen.speed(0)
        pen.color(color)
        return pen

    @staticmethod
    def set_turtle_pos(pen, pos_x, pos_y):
        pen.penup()
        pen.setpos(x=pos_x, y=pos_y)
        pen.pendown()
