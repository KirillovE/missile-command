import os
from TurtleHelper import Helper

HEALTH_LABEL_VERTICAL_INSET = -70


class Building:

    INITIAL_HEALTH = 10

    def __init__(self, window, pos_x, pos_y, name):
        self.window = window
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.name = name
        self.health = self.INITIAL_HEALTH

        self.health_points_label = self.create_health_points_label()
        self.health_points = self.health

        building_pen = self.create_building_pen()
        self.pen = building_pen
        self.set_shapes()

        building_pen.showturtle()

    def create_building_pen(self):
        building_pen = Helper.create_turtle('white')
        Helper.set_turtle_pos(building_pen, self.pos_x, self.pos_y)
        return building_pen

    def create_health_points_label(self):
        pen = Helper.create_turtle('white')
        Helper.set_turtle_pos(pen, self.pos_x, self.pos_y + HEALTH_LABEL_VERTICAL_INSET)
        pen.write(arg=self.health, align='center', font=['Arial', 20, 'bold'])

        return pen

    def set_shapes(self):
        picture_path = os.path.join(os.getcwd(), 'images', self.get_picture_name())
        if self.pen.shape() != picture_path:
            self.window.register_shape(picture_path)
            self.pen.shape(picture_path)
        if self.health != self.health_points:
            self.health_points = self.health
            self.health_points_label.clear()
            self.health_points_label.write(str(self.health_points), align='center', font=['Arial', 20, 'bold'])

    def get_picture_name(self):
        if self.health < self.INITIAL_HEALTH * 0.3:
            return f"{self.name}_3.gif"
        if self.health < self.INITIAL_HEALTH * 0.7:
            return f"{self.name}_2.gif"
        return f"{self.name}_1.gif"

    @property
    def is_alive(self):
        return self.health > 0


class MissileBase(Building):
    INITIAL_HEALTH = 30

    def get_picture_name(self):
        return f"{self.name}.gif"
