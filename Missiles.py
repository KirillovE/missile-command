import turtle
from TurtleHelper import Helper
from enum import Enum


class State(Enum):
    launched = 1
    exploding = 2
    dead = 3


class Missile:

    DETONATION_RADIUS = 20
    EXPLOSION_RADIUS = 5

    def __init__(self, pos_x, pos_y, color, target_x, target_y):
        self.color = color
        self.radius = 0
        self.target = target_x, target_y
        self.state = State.launched

        pen = Helper.create_turtle(color=self.color)
        Helper.set_turtle_pos(pen, pos_x, pos_y)
        self.set_heading(pen, self.target[0], self.target[1])
        pen.showturtle()

        self.pen = pen

    @staticmethod
    def set_heading(pen, target_x, target_y):
        heading = pen.towards(target_x, target_y)
        pen.setheading(heading)

    def step(self):
        if self.state == State.launched:
            self.pen.forward(4)

            if self.get_distance(from_x=self.target[0], from_y=self.target[1]) < self.DETONATION_RADIUS:
                self.explode()

        elif self.state == State.exploding:
            self.radius += 1

            if self.radius > self.EXPLOSION_RADIUS:
                self.evaporate()
            else:
                self.pen.shapesize(self.radius)

    def explode(self):
        self.pen.shape('circle')
        self.state = State.exploding

    def evaporate(self):
        self.pen.clear()
        self.pen.hideturtle()
        self.state = State.dead

    def get_distance(self, from_x, from_y):
        return self.pen.distance(x=from_x, y=from_y)

    @property
    def pos_x(self):
        return self.pen.xcor()

    @property
    def pos_y(self):
        return self.pen.ycor()
